<?php
include_once 'inc/opendb.php';
$token = md5(uniqid(rand(), TRUE));
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!-- head-common-meta -->
    <meta charset="UTF-8">
    <meta http-equiv=”x-ua-compatible” content=”IE=edge”/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- End head-common-meta -->
    <title>DStv | Derby</title>
    <!-- head-core-dependancies -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

    <link href="dist/css/main.css" rel="stylesheet" type="text/css">
    <link href="dist/css/animate.css" rel="stylesheet" type="text/css">
    <!-- End head-core-dependancies -->
</head>
<body>

<!-- Start main content area -->
<div id="hide-me"
     style="position: fixed; top: 0; left: 0px; width:100%; height:100%; z-index: 99; background-color: #000000;"></div>
<div class="header-logoes">
    <img src="dist/images/supersport-logo.png" alt="" class="logo-left">
    <img src="dist/images/dstv-compact-logo.png" alt="" class="logo-right">
</div>
<div id="page-structure">
    <div class="page-holder">

        <div class="rsvp loading page-content">
            <img class="mobi-img" src="dist/images/bg-1-mobi.jpg"/>
            <div class="rsvp-pane-align">
                <p>
                    Welcome! You have answered the calling, and are one step closer to getting your DStv Premiership prize courtesy of DStv Compact.
                </p>
                <p>
                    Please fill in and submit your unique code for your prize in the box below.
                </p>
                <form class="code-check" id="code-check" action="check.php" method="POST">
                    <div class="form-field">
                    <input id="voucher_code_check" name="code" class="code" minlength="6" maxlength="6" type="text"
                           oninvalid="this.setCustomValidity('Please enter your unique code')"
                           required placeholder="XXXXXX">
                    </div>
                    <input type="hidden" readonly name="csrf_token" id="csrf_token" value="<?php echo $token; ?>"/>
                    <button class="code-check-submit submit">SUBMIT</button>
                </form>
            </div>
        </div>

        <div class="landing messages loading page-content">
            <div class="holder">
                <p>
                    Congratulations on your DStv Premiership prize.
                </p>
                <p>
                    Soon, you could be rocking your very own custom Kaizer Chiefs
                    or Orlando Pirates football jersey.
                </p>
                <p>
                    Help us deliver your custom jersey to you, by filling in the details below.
                </p>
                <form class="entry-form" id="entry-form" action="details.php" method="POST">
                    <div class="form-field select">
                        <select name="fan_type" id="fan_type" required>
                            <option value="" selected>Are you a Cheifs or Pirates fan?</option>
                            <option value="Chiefs">Chiefs</option>
                            <option value="Pirates">Orlando Pirates</option>
                        </select>
                    </div>
                    <div class="form-field select">
                        <select name="jersey_size" id="jersey_size" required>
                            <option value="" selected>What is your jersey size?</option>
                            <option value="S">Small</option>
                            <option value="M">Medium</option>
                            <option value="L">Large</option>
                            <option value="XL">Xtra Large</option>
                            <option value="XXL">Xtra Xtra Large</option>
                        </select>
                    </div>
                    <div class="form-field">
                        <input id="name" name="name" minlength="3" maxlength="10" type="text" required placeholder="Name to be printed on the back">
                    </div>
                    <div class="form-field">
                        <input id="jersey_number" name="jersey_number" minlength="2" maxlength="2" type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required placeholder="Your number to be printed on the back">
                    </div>
                    <div class="form-field">
                        <textarea id="delivery_address" name="delivery_address" rows="4" cols="50" required placeholder="Your delivery address: unit number, street number and street name, building/complex name, Suburb, Area code, Province"></textarea>
                    </div>
                    <div class="form-field">
                        <input id="delivery_person" name="delivery_person" minlength="3" maxlength="10" type="text" required placeholder="Name of person to receive prize at delivery address">
                    </div>
                    <div class="form-field">
                        <input id="delivery_contact" name="delivery_contact" minlength="10" maxlength="10" type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required placeholder="Contact number of person to receive delivery">
                    </div>

                    <input type="hidden" readonly name="csrf_token" id="csrf_token" value="<?php echo $token; ?>"/>
                    <input type="hidden" readonly name="voucher_code" id="voucher_code" value="" />
                    <button class="entry-button submit">SUBMIT</button>

                </form>
            </div>

        </div>

        <div class="thank-you messages loading page-content">
            <img class="mobi-img" src="dist/images/bg-3-mobi.jpg"/>
            <div class="holder">
                <p class="yellow">Thank you!</p>
                <p>
                    Your details have been captured successfully.
                    Look out for a delivery coming your way soon.
                </p>
            </div>
        </div>

        <div class="error-code messages loading page-content">
            <img class="mobi-img" src="dist/images/bg-3-mobi.jpg"/>
            <div class="holder">
                <p class="yellow">Oh shucks - your unique code is not valid. </p>
                <p>
                    Please try again by entering the unique code you
                    received via SMS from DStv.
                </p>
                <button class="code-try-again submit">Try again</button>
            </div>
        </div>

        <div class="error-code-used messages loading page-content">
            <img class="mobi-img" src="dist/images/bg-3-mobi.jpg"/>
            <div class="holder">
                <p class="yellow">Oh shucks - code has already been used. </p>
                <p>
                    Please try again by entering the unique code you
                    received via SMS from DStv.
                </p>
                <button class="code-try-again submit">Try again</button>
            </div>
        </div>

        <div class="error-data-form messages loading page-content">
            <img class="mobi-img" src="dist/images/bg-3-mobi.jpg"/>
            <div class="holder">
                <p class="yellow">Oh shucks - something went wrong. </p>
                <p>
                    Please fill in all the information correctly.
                </p>
                <button class="code-try-again-main submit">Try again</button>
            </div>
        </div>

        <div class="terms messages loading page-content">
            <img class="mobi-img" src="dist/images/bg-1-mobi.jpg"/>
            <div class="holder">
                <p class="yellow">Terms and conditions: </p>
                <ul>
                    <li>Only recipients of the unique code from DStv will be able to claim an
                        Orlando Pirates or Kaizer Chiefs jersey.</li>
                    <li>Only one jersey per winner.</li>
                    <li>Delivery may take 5-7 working days within South Africa.</li>
                </ul>
                <button class="terms-go-back submit">Go back</button>
            </div>
        </div>

        <p class="terms"><span class="gotoTerms" href="javascript:void(0);">Ts&amp;Cs Apply</span></p>
    </div>
</div>
<!-- End main content area -->

<script src="dist/js/main.js" charset="utf-8"></script>
</body>
</html>

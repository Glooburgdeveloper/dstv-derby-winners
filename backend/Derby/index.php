<?php
//header("Location: https://www.dstv.co.za/");
//die();

include_once 'inc/opendb.php';

$token = md5(uniqid(rand(), TRUE));
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <!-- head-common-meta -->
    <meta charset="UTF-8">
  <meta http-equiv=”x-ua-compatible” content=”IE=edge” />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- End head-common-meta -->
    <title>DStv | The Lion King</title>
  <!-- head-core-dependancies -->
    <link href="dist/css/main.css" rel="stylesheet" type="text/css">
  <!-- End head-core-dependancies -->
</head>
<body>

<!-- Start main content area -->
<div id="hide-me" style="position: fixed; top: 0; left: 0px; width:100%; height:100%; z-index: 99; background-color: #000000;"></div>
<div id="page-structure">
  <div class="page-holder">
    <div class="rsvp loading">
      <img class="mobi-img" src="dist/assets/img/background-m.jpg"/>
      <div class="rsvp-pane-align">
      <h1><strong><i>Be among the first in Africa to see The Lion King at an exclusive premiere.<br>Sign up to get DStv Premium for just  R749pm x 24 months, for your chance to win.</i></strong></h1>

		  <form class="entry-form" id="validate-code" action="check.php" method="POST">
			  <h1>Unique Code</h1>
			  <input id="voucher_code_check" name="code" minlength="6" type="text" required placeholder="Unique Code">
			  <input type="hidden" readonly name="csrf_token" id="csrf_token" value="<?php echo $token; ?>" />
			  <p style="text-align: center;">
				  <button class="entry-button code-entry-button submit">
					  &nbsp;&nbsp;&nbsp;SUBMIT&nbsp;&nbsp;&nbsp;
				  </button>
			  </p>
		  </form>
      </div>
    </div>
    <div class="landing loading">
        <img class="mobi-img" src="dist/assets/img/background-alt-m.jpg"/>
        <form class="entry-form" id="entry-form" action="details.php" method="POST">
          <h1>YOUR DETAILS</h1>
        <select class="" name="fan_type">
			<option value="">select</option>
			<option value="Chiefs">Chiefs</option>
			<option value="Pirates">Pirates</option>
		</select>
          <br>
			<select class="" name="jersey_size">
				<option value="">select</option>
				<option value="small">small</option>
				<option value="medium">medium</option>
				<option value="large">large</option>
				<option value="xl">XL</option>
				<option value="xxl">XXL</option>
			</select>
          <br>
          <input id="name" name="name" type="text" maxlength="10" required placeholder="NAME">
          <br>
          <input id="jersey_number" name="jersey_number" minlength="2" maxlength="2" type="number" required placeholder="jersey_number">
          <br>
          <input id="delivery_address" name="delivery_address" minlength="1" type="text" required placeholder="delivery_address">
          <br>
			<input id="delivery_person" name="delivery_person" minlength="1" type="text" required placeholder="delivery_person">
			<br>
			<input id="delivery_contact" name="delivery_contact" type="number" required placeholder="delivery_contact">
			<br>
          <input type="hidden" readonly name="csrf_token" id="csrf_token" value="<?php echo $token; ?>" />
          <input type="text"  readonly name="code" id="voucher_code" value="" />
          <p style="text-align: center;">
            <button class="entry-button submit">
              &nbsp;&nbsp;&nbsp;SUBMIT&nbsp;&nbsp;&nbsp;
            </button>
          </p>
        </form>
    </div>

	  <div class="invalid-code">
		  <img class="mobi-img" src="dist/assets/img/background-alt-m.jpg"/>
		  <div class="invalid-code-holder">
			  <h1><strong>INVALID CODE</strong></h1>
			  <p>please try again,
				  The DStv team</p>
		  </div>
	  </div>


    <div class="thank-you loading">
      <img class="mobi-img" src="dist/assets/img/background-alt-m.jpg"/>
      <div class="thank-you-holder">
        <h1><strong>THANK YOU</strong></h1>
        <p>You've successfully captured your details. We'll be in touch within 48 - 72 hours,
          The DStv team</p>
      </div>
    </div>
    <p class="terms"><a href="terms.pdf" target="_blank">Ts&amp;Cs Apply</a></p>
  </div>
</div>
<!-- End main content area -->

  <script src="dist/js/main.js" charset="utf-8"></script>
</body>
</html>

<?php
header("Access-Control-Allow-Origin: *");

include 'inc/opendb.php';

if(!$_POST['fan_type'])
{
	header('X-Error-Message: Please select your fan type', true, 400);
	ajaxJsonOutput('error','Please select your fan type');

}
if(!$_POST['jersey_size'])
{
	header('X-Error-Message: Please select your fan type', true, 400);
	ajaxJsonOutput('error','Please select your size');

}

if(!$_POST['name'])
{

	header('X-Error-Message: Please enter your name', true, 400);
	ajaxJsonOutput('error','Please enter your name');
	die();
}

if(strlen($_POST['name'])>10)
{

	header('X-Error-Message: Please enter valid name', true, 400);
	ajaxJsonOutput('error','Please enter valid name');
	die();
}

if(!is_numeric($_POST['jersey_number']))
{

header('X-Error-Message: Please enter valid number', true, 400);
ajaxJsonOutput('error','Please enter valid number');
die();
}

if(!$_POST['delivery_address'])
{

	header('X-Error-Message: Please enter your delivery address', true, 400);
	ajaxJsonOutput('error','Please enter your delivery address');
	die();
}
if(!$_POST['delivery_person'])
{

	header('X-Error-Message: Please enter your delivery person', true, 400);
	ajaxJsonOutput('error','Please enter your delivery person');
	die();
}
if(!$_POST['delivery_contact'])
{

	header('X-Error-Message: Please enter your delivery contact', true, 400);
	ajaxJsonOutput('error','Please enter your delivery contact');
	die();
}
if(!$_POST['code'])
{

	header('X-Error-Message: Please enter your code', true, 400);
	ajaxJsonOutput('error','Please enter your code');
	die();
}

if(count($_POST)>0)
{
	if ($_POST['csrf_token'] == $_SESSION['token'])
	{
		/* Valid Token */
		$token_age = time() - $_SESSION['token_time'];
		if ($token_age <= 900)
		{

			//first check code is valid


			$query = "select * from derby_voucher_codes where voucher_key=?";
			$statement = $mysqli->prepare($query);


			//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
			$statement->bind_param('s', $_POST['code']);

			if($statement->execute())
			{
				$result = $statement->get_result();
				$r      = $result->fetch_assoc();

				if($r)
				{

					if($r['used']==1)
					{
						header('X-Error-Message: updated successfully', true, 200);
						ajaxJsonOutput('used_already','fetched successfully',$r);
					}
					else
					{
						$voucher_code_id=$r['voucher_id'];
						//update code is used
						$query = "update derby_voucher_codes  set used=1 where voucher_key=?";
						$statement = $mysqli->prepare($query);


						//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
						$statement->bind_param('s', $_POST['code']);

						if($statement->execute())
						{
//enter
							/* Less than 15 minutes has passed. */
							$query = "INSERT INTO  derby_leads(fan_type,jersey_size,name,jersey_number,delivery_address,delivery_person_name,delivery_person_contact_no,voucher_code_id) values(?,?,?,?,?,?,?,?)";
							$statement = $mysqli->prepare($query);


							//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
							$statement->bind_param('ssssssss', $_POST['fan_type'],$_POST['jersey_size'],$_POST['name'],$_POST['jersey_number'],$_POST['delivery_address'],$_POST['delivery_person'],$_POST['delivery_contact'],$voucher_code_id);

							if($statement->execute())
							{
								$id=$mysqli->insert_id;
								header('X-Error-Message: updated successfully', true, 200);
								ajaxJsonOutput('success','updated successfully');
							}
							else
							{
								//die('Error : ('. $mysqli->errno .') '. $mysqli->error);
								header('X-Error-Message: Server Error Occurred Please try again later', true, 500);
								ajaxJsonOutput('error','Server Error Occurred Please try again later 1','reload');
								//die('Error : ('. $mysqli->errno .') '. $mysqli->error);
							}

						}
						else{
							die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						}


					}

				}
				else
				{
					header('X-Error-Message: Server Error Occurred Please try again later 2', true, 400);
					ajaxJsonOutput('not_exist','code not exist');
				}


			}
			else
			{
				header('X-Error-Message: Server Error Occurred Please try again later', true, 500);
				ajaxJsonOutput('error','Server Error Occurred Please try again later 3','reload');
				//die('Error : ('. $mysqli->errno .') '. $mysqli->error);
			}

			$statement->close();

		}
		else
			header('X-Error-Message: Session time expired', true, 400);
		ajaxJsonOutput('error','Server Error Occurred Please try again later 4','reload');
	}
	else
	{
		header('X-Error-Message: Session token mismatched,please try again', true, 400);
		ajaxJsonOutput('error','Session token mismatched'.$_POST['csrf_token'].' '.$_SESSION['token'],'reload');

	}


}
else
{
	header('X-Error-Message: Input error', true, 400);
	ajaxJsonOutput('error','No post data');
}



?>
<?php
header("Access-Control-Allow-Origin: *");

include 'inc/opendb.php';

if(!$_POST['code'])
{
	header('X-Error-Message: Please enter your name', true, 400);
	ajaxJsonOutput('error','Please enter your name');

}
if(count($_POST)>0)
{
	if ($_POST['csrf_token'] == $_SESSION['token'])
	{
		/* Valid Token */
		$token_age = time() - $_SESSION['token_time'];
		if ($token_age <= 900)
		{

			/* Less than 15 minutes has passed. */
			$query = "select * from derby_voucher_codes where voucher_key=?";
			$statement = $mysqli->prepare($query);


			//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
			$statement->bind_param('s', $_POST['code']);

			if($statement->execute())
			{
				$result  = $statement->get_result();
				$r       = $result->fetch_assoc();

				if($r)
				{

					if($r['used']==1)
					{
						header('X-Error-Message: updated successfully', true, 200);
						ajaxJsonOutput('used_already','fetched successfully',$r);
					}
					else{
						header('X-Error-Message: updated successfully', true, 200);
						ajaxJsonOutput('success','fetched successfully',$r);
					}

				}
				else
				{
					header('X-Error-Message: Server Error Occurred Please try again later', true, 400);
					ajaxJsonOutput('not_exist','code not exist');
				}

			}
			else
			{
				header('X-Error-Message: Server Error Occurred Please try again later', true, 500);
				ajaxJsonOutput('error','Server Error Occurred Please try again later','reload');
				//die('Error : ('. $mysqli->errno .') '. $mysqli->error);
			}
			$statement->close();

		}
		else
			header('X-Error-Message: Session time expired', true, 400);
		ajaxJsonOutput('error','Server Error Occurred Please try again later','reload');
	}
	else
	{
		header('X-Error-Message: Session token mismatched,please try again', true, 400);
		ajaxJsonOutput('error','Session token mismatched'.$_POST['csrf_token'].' '.$_SESSION['token'],'reload');

	}


}
else
{
	header('X-Error-Message: Input error', true, 400);
	ajaxJsonOutput('error','No post data');
}



?>
-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2017 at 11:29 AM
-- Server version: 5.7.11
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dstv_moments`
--

-- --------------------------------------------------------

--
-- Table structure for table `mssession`
--

DROP TABLE IF EXISTS `mssession`;
CREATE TABLE `mssession` (
  `id` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `last_access` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mssession`
--

INSERT INTO `mssession` (`id`, `data`, `last_access`) VALUES
('kisg0nu1v01gfao33r8p6k9lk3', '', 1499858900);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_ref` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `user_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `user_paid_first_month` tinyint(1) NOT NULL DEFAULT '0',
  `user_paid_first_month_date` date NOT NULL,
  `user_paid_second_month` tinyint(1) NOT NULL DEFAULT '0',
  `user_paid_second_month_date` date NOT NULL,
  `user_paid_third_month` tinyint(1) NOT NULL DEFAULT '0',
  `user_paid_third_month_date` date NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mssession`
--
ALTER TABLE `mssession`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;